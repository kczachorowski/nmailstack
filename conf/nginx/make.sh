#!/bin/bash
set -e

rm -f /etc/nginx/sites-enabled/default

CMD="install -o root -g root -m 644"
$CMD webmail.conf /etc/nginx/sites-available/webmail.conf
$CMD postfixadmin.conf /etc/nginx/sites-available/postfixadmin.conf

cd /etc/nginx/sites-enabled
ln -sf ../sites-available/webmail.conf .
ln -sf ../sites-available/postfixadmin.conf
