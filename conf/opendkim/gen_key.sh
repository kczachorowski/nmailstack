#!/bin/bash
set -e

gen_key() {
    SELECTOR="$1"
    DOMAIN="$2"
    FBASE="${SELECTOR}_${DOMAIN}"
    FDNS=${FBASE}.txt
    FKEY=${FBASE}.private

    test -f /etc/dkimkeys/$FKEY && return 0

    tmpd=$(mktemp -d)
    opendkim-genkey -d $DOMAIN -s $SELECTOR -D $tmpd
    install -o root -g opendkim -m 640 ${tmpd}/${SELECTOR}.private /etc/dkimkeys/${FKEY}
    install -o root -g opendkim -m 640 ${tmpd}/${SELECTOR}.txt /etc/dkimkeys/${FDNS}
    rm -f ${tmpd}/${SELECTOR}.private ${tmpd}/${SELECTOR}.txt

    # keytable
    #mail._domainkey.example.com example.com:mail:/etc/postfix/dkim/mail.private 
    echo "${SELECTOR}._domainkey.${DOMAIN} ${DOMAIN}:${SELECTOR}:/etc/dkimkeys/${FKEY}" >> /etc/opendkim/keytable

    # signingtable
    # *@example.com mail._domainkey.example.com
    echo "*@${DOMAIN} ${SELECTOR}._domainkey.${DOMAIN}" >> /etc/opendkim/signingtable
    rmdir ${tmpd}
}

gen_key "$1" "$2"
