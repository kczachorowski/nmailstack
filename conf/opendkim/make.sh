#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

UMASK=644 touch /etc/opendkim/keytable
UMASK=644 touch /etc/opendkim/signingtable
UMASK=755 mkdir -p /etc/opendkim

CMD="install -o root -g root -m 640"
$CMD opendkim.conf /etc/opendkim.conf
install -o root -g root -m 644 trustedhosts /etc/opendkim/trustedhosts

bash gen_key.sh mail $MAILNAME
