#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

CMD="install -o root -g amavis -m 640"
$CMD conf.d/05-node_id /etc/amavis/conf.d/05-node_id
$CMD conf.d/15-content_filter_mode /etc/amavis/conf.d/15-content_filter_mode
$CMD conf.d/20-network /etc/amavis/conf.d/20-network
$CMD conf.d/50-dkim /etc/amavis/conf.d/50-dkim

touch /etc/amavis/conf.d/55-postfix
chown root:amavis /etc/amavis/conf.d/55-postfix
chmod 640 /etc/amavis/conf.d/55-postfix

cat conf.d/55-postfix | sed \
    -e "s/DB_POSTFIX_DB/$DB_POSTFIX_DB/g" \
    -e "s/DB_POSTFIX_USER/$DB_POSTFIX_USER/g" \
    -e "s/DB_POSTFIX_PASS/$DB_POSTFIX_PASS/g" \
    -e "s/DEFAULT_PASS_SCHEME/$DEFAULT_PASS_SCHEME/g" \
> /etc/amavis/conf.d/55-postfix
