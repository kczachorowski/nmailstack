#!/bin/bash
set -e

if [ 1=1 ]; then
  if ! [ -e index.php -a -e bin/installto.sh ]; then
    echo >&2 "roundcubemail not found in $PWD - copying now..."
    if [ "$(ls -A)" ]; then
      echo >&2 "WARNING: $PWD is not empty - press Ctrl+C now if this is an error!"
      ( set -x; ls -A; sleep 1 )
    fi
    tar cf - --one-file-system -C /usr/src/roundcubemail . | tar xf -
    rm -rf installer index.html
    echo >&2 "Complete! ROUNDCUBEMAIL has been successfully copied to $PWD"
  fi

  if [ ! -z "${!POSTGRES_ENV_POSTGRES_*}" ] || [ "$ROUNDCUBEMAIL_DB_TYPE" == "pgsql" ]; then
    : "${ROUNDCUBEMAIL_DB_TYPE:=pgsql}"
    : "${ROUNDCUBEMAIL_DB_HOST:=192.168.216.1}"
    : "${ROUNDCUBEMAIL_DB_PORT:=5432}"
#    : "${ROUNDCUBEMAIL_DB_USER:=${POSTGRES_ENV_POSTGRES_USER}}"
#    : "${ROUNDCUBEMAIL_DB_PASSWORD:=${POSTGRES_ENV_POSTGRES_PASSWORD}}"
#    : "${ROUNDCUBEMAIL_DB_NAME:=${POSTGRES_ENV_POSTGRES_DB:-roundcubemail}}"
#    : "${ROUNDCUBEMAIL_DSNW:=${ROUNDCUBEMAIL_DB_TYPE}://${ROUNDCUBEMAIL_DB_USER}:${ROUNDCUBEMAIL_DB_PASSWORD}@${ROUNDCUBEMAIL_DB_HOST}/${ROUNDCUBEMAIL_DB_NAME}}"
    timeout 15 bash -c "until echo > /dev/tcp/${ROUNDCUBE_DB_HOST}/${ROUNDCUBE_DB_PORT}; do sleep 0.5; done"
  fi

#  : "${ROUNDCUBEMAIL_DEFAULT_HOST:=localhost}"
#  : "${ROUNDCUBEMAIL_DEFAULT_PORT:=143}"
#  : "${ROUNDCUBEMAIL_SMTP_SERVER:=localhost}"
#  : "${ROUNDCUBEMAIL_SMTP_PORT:=587}"
#  : "${ROUNDCUBEMAIL_PLUGINS:=archive,zipdownload}"
#  : "${ROUNDCUBEMAIL_TEMP_DIR:=/tmp/roundcube-temp}"

    # initialize DB
    bin/initdb.sh --dir=$PWD/SQL || bin/updatedb.sh --dir=$PWD/SQL --package=roundcube || echo "Failed to initialize databse. Please run $PWD/bin/initdb.sh manually."
fi

exec "$@"
