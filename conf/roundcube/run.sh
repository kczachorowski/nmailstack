#!/bin/bash
set -e

docker stop -t0 roundcube || true
docker rm roundcube || true

docker run -d \
    -p 1080:8000 \
    --name=roundcube \
    --network=vmail \
    --restart=unless-stopped \
    -v /etc/roundcube/config.inc.php:/var/www/html/config/config.inc.php \
    -v /etc/roundcube/managesieve__config.inc.php:/var/www/html/plugins/managesieve/config.inc.php \
    -v /etc/roundcube/password__config.inc.php:/var/www/html/plugins/password/config.inc.php \
    roundcube-image
#    --log-driver=syslog --log-opt syslog-address=tcp://127.0.0.1:514 --log-opt tag=vortale \
