#!/bin/bash
set -e

mkdir -p /etc/roundcube
chmod 0750 /etc/roundcube
chown root:root /etc/roundcube

install -o root -g root -m 750 run.sh /etc/roundcube/run.sh

test -f /etc/roundcube/secret || {
    touch /etc/roundcube/secret
    chmod 0640 /etc/roundcube/secret
    chown root:root /etc/roundcube/secret
    SECRET=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    echo "\$config['des_key'] = '$SECRET';" > /etc/roundcube/secret
}


echo "\
config.inc.php
managesieve__config.inc.php
password__config.inc.php" |\
while read f
do
    ../prepare_config.sh $f > /etc/roundcube/$f
done

cat /etc/roundcube/secret >> /etc/roundcube/config.inc.php
test -f /etc/roundcube/config.local.php && cat /etc/roundcube/config.local.php >> /etc/roundcube/config.inc.php

docker pull narel/php:7
docker build -t roundcube-image docker/
