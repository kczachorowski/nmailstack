#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

CMD="install -o root -g root -m 644"
$CMD default /etc/default/spamassassin

touch /etc/spamassassin/local.cf
chmod 640 /etc/spamassassin/local.cf
chown root:root /etc/spamassassin/local.cf
../prepare_config.sh local.cf > /etc/spamassassin/local.cf

TYPE="postgresql" HOST="$DB_SPAM_HOST" DATABASE="$DB_SPAM_DB" LOGIN="$DB_SPAM_USER" PASSWORD="$DB_SPAM_PASS" PORT="$DB_SPAM_PORT" ../shmig up

systemctl restart spamassassin
