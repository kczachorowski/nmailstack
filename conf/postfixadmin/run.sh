#!/bin/bash
set -e

docker stop -t0 postfixadmin || true
docker rm postfixadmin || true

docker run -d \
    -p 1081:8000 \
    --name=postfixadmin \
    --network=vmail \
    --restart=unless-stopped \
    -v /etc/postfixadmin/config.inc.php:/var/www/html/config.local.php \
    postfixadmin-image
#    --log-driver=syslog --log-opt syslog-address=tcp://127.0.0.1:514 --log-opt tag=vortale \
