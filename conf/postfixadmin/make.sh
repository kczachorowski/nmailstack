#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

mkdir -p /etc/postfixadmin
chmod 0750 /etc/postfixadmin
chown root:root /etc/postfixadmin

install -o root -g root -m 750 run.sh /etc/postfixadmin/run.sh

touch /etc/postfixadmin/config.inc.php
chown 1000:1000 /etc/postfixadmin/config.inc.php
chmod 640 /etc/postfixadmin/config.inc.php

../prepare_config.sh config.inc.php > /etc/postfixadmin/config.inc.php

test -f /etc/postfixadmin/config.local.php && cat /etc/postfixadmin/config.local.php >> /etc/postfixadmin/config.inc.php

docker pull narel/php:7
docker build -t postfixadmin-image docker/
