<?php

$CONF['configured'] = true;
$CONF['default_language'] = 'pl';
$CONF['database_type'] = 'pgsql';
$CONF['database_host'] = '192.168.216.1';
$CONF['database_user'] = '$DB_POSTFIX_USER';
$CONF['database_password'] = '$DB_POSTFIX_PASS';
$CONF['database_name'] = '$DB_POSTFIX_DB';
$CONF['admin_email'] = '$ADMIN_EMAIL';
$CONF['smtp_server'] = '192.168.216.1';
$CONF['encrypt'] = '$DOVECOT_ENCRYPT';


$CONF['password_validation'] = array(
#    '/regular expression/' => '$PALANG key (optional: + parameter)',
    '/.{8}/'                => 'password_too_short 8',      # minimum length 8 characters
    '/([a-zA-Z].*){3}/'     => 'password_no_characters 3',  # must contain at least 3 characters
    '/([0-9].*){1}/'        => 'password_no_digits 1',      # must contain at least 1 digits
);


$CONF['generate_password'] = 'YES';
$CONF['show_password'] = 'YES';
$CONF['page_size'] = '30';

$CONF['default_aliases'] = array (
    'abuse' => 'admin@$MAILNAME',
    'hostmaster' => 'admin@$MAILNAME',
    'postmaster' => 'admin@$MAILNAME',
    'webmaster' => 'admin@$MAILNAME'
);



$CONF['aliases'] = '20';
$CONF['mailboxes'] = '20';
$CONF['maxquota'] = '1024';
$CONF['domain_quota_default'] = '2048';

// Quota
// When you want to enforce quota for your mailbox users set this to 'YES'.
$CONF['quota'] = 'YES';
// If you want to enforce domain-level quotas set this to 'YES'.
$CONF['domain_quota'] = 'YES';
// You can either use '1024000' or '1048576'
$CONF['quota_multiplier'] = '1024000';

// Show used quotas from Dovecot dictionary backend in virtual
// mailbox listing.
$CONF['used_quotas'] = 'YES';
$CONF['new_quota_table'] = 'YES';



$CONF['fetchmail'] = 'NO';

$CONF['show_footer_text'] = 'NO';

$CONF['welcome_text'] = <<<EOM
Witaj,

Twoje konto jest aktywne!
Konfiguracja klienta poczty to:
serwer IMAP/POP3: $MAILNAME
wymagany SSL, jako login podaj adres email.
serwer SMTP: $MAILNAME, port 587
wymagany TLS, serwer wymaga uwierzytelnienia.
EOM;

$CONF['emailcheck_resolve_domain']='NO';

//
// END OF CONFIG FILE
//
/* vim: set expandtab softtabstop=4 tabstop=4 shiftwidth=4: */
