#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

CMD="install -o root -g dovecot -m 640"

$CMD ./conf.d/10-auth.conf /etc/dovecot/conf.d/10-auth.conf
$CMD ./conf.d/10-director.conf /etc/dovecot/conf.d/10-director.conf
$CMD ./conf.d/10-tcpwrapper.conf /etc/dovecot/conf.d/10-tcpwrapper.conf
$CMD ./conf.d/10-logging.conf /etc/dovecot/conf.d/10-logging.conf
$CMD ./conf.d/10-mail.conf /etc/dovecot/conf.d/10-mail.conf
$CMD ./conf.d/10-master.conf /etc/dovecot/conf.d/10-master.conf
$CMD ./conf.d/10-ssl.conf /etc/dovecot/conf.d/10-ssl.conf
$CMD ./conf.d/15-lda.conf /etc/dovecot/conf.d/15-lda.conf
$CMD ./conf.d/15-mailboxes.conf /etc/dovecot/conf.d/15-mailboxes.conf
$CMD ./conf.d/20-imap.conf /etc/dovecot/conf.d/20-imap.conf
$CMD ./conf.d/20-lmtp.conf /etc/dovecot/conf.d/20-lmtp.conf
$CMD ./conf.d/20-managesieve.conf /etc/dovecot/conf.d/20-managesieve.conf
$CMD ./conf.d/20-pop3.conf /etc/dovecot/conf.d/20-pop3.conf
$CMD ./conf.d/90-acl.conf /etc/dovecot/conf.d/90-acl.conf
$CMD ./conf.d/90-expire.conf /etc/dovecot/conf.d/90-expire.conf
$CMD ./conf.d/90-plugin.conf /etc/dovecot/conf.d/90-plugin.conf
$CMD ./conf.d/90-sieve.conf /etc/dovecot/conf.d/90-sieve.conf
$CMD ./conf.d/90-sieve-extprograms.conf /etc/dovecot/conf.d/90-sieve-extprograms.conf
$CMD ./conf.d/90-quota.conf /etc/dovecot/conf.d/90-quota.conf
$CMD ./conf.d/auth-dict.conf.ext /etc/dovecot/conf.d/auth-dict.conf.ext
$CMD ./conf.d/auth-system.conf.ext /etc/dovecot/conf.d/auth-system.conf.ext

install -o root -g root -m 644 dovecot-trash.conf.ext /etc/dovecot/dovecot-trash.conf.ext

# sieve
mkdir -p /var/lib/dovecot/sieve && chown root:mail /var/lib/dovecot/sieve && chmod 775 /var/lib/dovecot/sieve
install -o root -g root -m 644 sieve/report-ham.sieve /var/lib/dovecot/sieve/report-ham.sieve
install -o root -g root -m 644 sieve/report-spam.sieve /var/lib/dovecot/sieve/report-spam.sieve
install -o root -g root -m 755 sieve/sa-learn-ham.sh /var/lib/dovecot/sieve/sa-learn-ham.sh
install -o root -g root -m 755 sieve/sa-learn-spam.sh /var/lib/dovecot/sieve/sa-learn-spam.sh

touch /etc/dovecot/dovecot-sql.conf.ext
chown root:dovecot /etc/dovecot/dovecot-sql.conf.ext
chmod 640 /etc/dovecot/dovecot-sql.conf.ext
../prepare_config.sh dovecot-sql.conf.ext > /etc/dovecot/dovecot-sql.conf.ext


touch /etc/dovecot/dovecot-dict-sql-user.conf
chown root:dovecot /etc/dovecot/dovecot-dict-sql-user.conf
chmod 640 /etc/dovecot/dovecot-dict-sql-user.conf
../prepare_config.sh dovecot-dict-sql-user.conf > /etc/dovecot/dovecot-dict-sql-user.conf

touch /etc/dovecot/dovecot-dict-sql-domain.conf
chown root:dovecot /etc/dovecot/dovecot-dict-sql-domain.conf
chmod 640 /etc/dovecot/dovecot-dict-sql-domain.conf
../prepare_config.sh dovecot-dict-sql-domain.conf > /etc/dovecot/dovecot-dict-sql-domain.conf
