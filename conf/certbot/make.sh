#!/bin/bash

set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

if [ ! -f /etc/letsencrypt/live/$MAILNAME/privkey.pem ]
then
    systemctl stop nginx
    certbot certonly -n --standalone -d $MAILNAME || FAIL=1
    systemctl start nginx
    test -f /etc/letsencrypt/live/$MAILNAME/privkey.pem && ln -sf /etc/letsencrypt/live/$MAILNAME/privkey.pem /etc/ssl/private/mail.key
    test -f /etc/letsencrypt/live/$MAILNAME/fullchain.pem && ln -sf /etc/letsencrypt/live/$MAILNAME/fullchain.pem /etc/ssl/private/mail.pem
fi

