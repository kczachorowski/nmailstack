#!/bin/bash
set -e

RESTART=false

grep -q '# LISTEN NETWORKS$' /etc/postgresql/9.6/main/postgresql.conf || {
    echo "Modifying postgresql configuration"
    echo "listen_addresses = '127.0.0.1, 192.168.216.1' # LISTEN NETWORKS" >> /etc/postgresql/9.6/main/postgresql.conf
    echo "    done"
    RESTART=true
    sleep 5
}

grep -q '^host all all 0.0.0.0/0 md5$' /etc/postgresql/9.6/main/pg_hba.conf || {
    echo "Modifying postgresql configuration"
    echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/9.6/main/pg_hba.conf
    echo "    done"
    RESTART=true
    sleep 5
}

[ "$RESTART" = "true" ] && {
    echo "Restarting postgresql"
    systemctl restart postgresql
}

su - postgres -c 'createuser -s root || true'

test -f /root/MAIL_ENV || { echo "/root/MAIL_ENV does not exists"; exit 1; }
set -x

source /root/MAIL_ENV
createuser $DB_POSTFIX_USER || true
createdb --owner=$DB_POSTFIX_USER $DB_POSTFIX_DB || true
createuser $DB_WEBMAIL_USER || true
createdb --owner=$DB_WEBMAIL_USER $DB_WEBMAIL_DB || true
createuser $DB_SPAM_USER || true
createdb --owner=$DB_SPAM_USER $DB_SPAM_DB || true

cat << EOF | psql template1
    ALTER USER $DB_POSTFIX_USER WITH PASSWORD '$DB_POSTFIX_PASS';
    ALTER USER $DB_WEBMAIL_USER WITH PASSWORD '$DB_WEBMAIL_PASS';
    ALTER USER $DB_SPAM_USER WITH PASSWORD '$DB_SPAM_PASS';
EOF
