#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

bash db_user.sh
