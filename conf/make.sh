#!/bin/bash
set -e

check_hash () {
    # usage: install_file SRC DST

    if [ -f $2 ]; then
        cur_hash=$(sha1sum "$2")

    if [ -f ${2}.sha1 ]
    then
        old_hash=$(cat "${2}.sha1")
    else
        old_hash=$cur_hash
        echo "$cur_hash" > ${2}.sha1
    fi

    if [ "$old_hash" = "$cur_hash" ]
    then
        return 0
    else
        return 1
    fi
}


check_hash /tmp/dupa /tmp/kupa
