#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

# policyd-spf
echo "Installing /etc/postfix-policyd-spf-python/policyd-spf.conf"
install -o root -g root -m 644 policyd-spf.conf /etc/postfix-policyd-spf-python/policyd-spf.conf

echo "Installing /etc/postfix/auth_header_checks.pcre"
#install -o root -g root -m 644 header_checks /etc/postfix/header_checks
../prepare_config.sh auth_header_checks.pcre > /etc/postfix/auth_header_checks.pcre
chmod 644 /etc/postfix/auth_header_checks.pcre
chown root: /etc/postfix/auth_header_checks.pcre

# mailname
echo "Updating /etc/mailname"
echo "$MAILNAME" > /etc/mailname

# sql
echo
echo "Installing sql config files"
mkdir -p /etc/postfix/sql
cd sql-template
bash ./make.sh
install -o root -g postfix -m 640 -b sql/*.cf /etc/postfix/sql/
echo "    done"
cd - > /dev/null

# main.cf
echo
echo "Installing main.cf"
mkdir -p /etc/postfix/main.cf.d/
install -o root -g root -m 644 main.cf.d/*.cf /etc/postfix/main.cf.d/
install -o root -g root -m 750 main.cf.d/make.sh /etc/postfix/main.cf.d/
cd /etc/postfix/main.cf.d/
./make.sh
echo "    done"
cd - > /dev/null

# master.cf
echo
echo "Installing master.cf"
touch /etc/postfix/master.cf
cur_hash=$(sha1sum /etc/postfix/master.cf)
if [ -f /etc/postfix/master.cf.sha1 ]
then
    old_hash=$(cat /etc/postfix/master.cf.sha1)
else
    old_hash=$cur_hash
    echo "$new_hash" > /etc/postfix/master.cf
fi

if [ "$old_hash" = "$cur_hash" ]
then
    # file master.cf was not modified on the server
    echo "   done"
    install -o root -g root -m 644 -b master.cf /etc/postfix/master.cf
    sha1sum /etc/postfix/master.cf > /etc/postfix/master.cf.sha1
else
    echo "WARNING: file master.cf was modified on the server!!! I will not update it!"
    install -o root -g root -m 644 master.cf /etc/postfix/master.cf.upstream
fi

postmap /etc/postfix/header_checks
touch /etc/postfix/transport
postmap /etc/postfix/transport
touch /etc/postfix/whitelist_limit
postmap /etc/postfix/whitelist_limit

systemctl restart postfix
