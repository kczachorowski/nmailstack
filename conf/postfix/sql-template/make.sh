#!/bin/bash
set -e

source /root/MAIL_ENV || { echo "No /root/MAIL_ENV file!!!"; exit 1; }

rm -rf sql
mkdir sql
 
for i in *.tmpl
do
    echo "Processing $i file"
    fname=$(basename $i .tmpl)
    cat << EOF > sql/$fname
user=$DB_POSTFIX_USER
password=$DB_POSTFIX_PASS
hosts=127.0.0.1
dbname=$DB_POSTFIX_DB
port=5432

EOF

    cat $i >> sql/$fname
done
