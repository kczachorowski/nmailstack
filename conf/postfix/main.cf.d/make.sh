#!/bin/bash

source /root/MAIL_ENV || { echo "ERROR: No /root/MAIL_ENV file found!"; exit 1; }

echo "myhostname = $MAILNAME" > /etc/postfix/main.cf
cat *.cf >> /etc/postfix/main.cf
