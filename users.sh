#!/bin/bash
set -e

check_user() {
    return $(getent passwd "$1" > /dev/null)
}

check_group() {
    return $(getent group "$1" > /dev/null)
}

check_user postfix || { groupadd -g 200 postfix && useradd -g postfix -u 200 postfix -d /var/spool/postfix -s /bin/false; }
check_user dovecot || { groupadd -g 201 dovecot && useradd -g dovecot -u 201 dovecot -d /usr/lib/dovecot -s /bin/false; }
check_user dovenull || { groupadd -g 202 dovenull && useradd -g dovenull -u 202 dovenull -d /nonexistent -s /bin/false; }
check_user amavis || { groupadd -g 203 amavis && UMASK=750 useradd -g amavis -u 203 amavis -d /var/lib/amavis --create-home -s /bin/sh; }
check_user clamav || { groupadd -g 204 clamav && UMASK=755 useradd -g clamav -u 204 clamav -d /var/lib/clamav --create-home -s /bin/false; }
check_user debian-spamd || { groupadd -g 205 debian-spamd && useradd -g debian-spamd -u 205 debian-spamd -d /var/lib/spamassassin -s /bin/sh; } 
check_user postgrey || { groupadd -g 206 postgrey && UMASK=700 useradd -g postgrey -u 206 postgrey -d /var/lib/postgrey --create-home -s /bin/false; }
check_user policyd-spf || { groupadd -g 207 policyd-spf && useradd -g policyd-spf -u 207 policyd-spf -d /nonexistent -s /bin/false; }
check_user opendkim || { groupadd -g 208 opendkim && UMASK=750 useradd -g opendkim -u 208 opendkim -d /var/run/opendkim --create-home -s /bin/false; }
check_user vmail || { groupadd -g 500 vmail && useradd -g vmail -u 500 vmail -d /srv/vmail -m -s /bin/false; }
check_group postdrop || { groupadd -g 210 postdrop; }
adduser amavis clamav || adduser clamav amavis

