#!/bin/bash

CONF="/root/MAIL_ENV"

prepare_config() {
    FILE="$1"
    test -f $FILE || exit 1
    test -f $CONF || exit 1
    VARLIST=$(cat $CONF |grep '^export [A-Z]'| sed -e 's/export //' | awk 'BEGIN { FS="="; ORS="," }; END{sub(/,$/, "", l)}; {print "$"$1}'|sed -e 's/,$//')
    source $CONF > /dev/null
    envsubst $VARLIST < $FILE
}

prepare_config "$1"
