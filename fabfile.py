from fabric.api import hosts, env, run, get
from fabric.operations import put
from fabric.contrib.project import rsync_project
from fabric.contrib.files import exists
from fabric.context_managers import cd
from fabric.operations import local as lrun
from fabric.operations import sudo
import datetime
import os


INSTALL_ROOT = '/tmp/__mdeploy'


def __upload(target):
    run('sudo rm -rf {0} && mkdir {0} && chmod 700 {0}'.format(INSTALL_ROOT))
    put('prepare_config.sh', INSTALL_ROOT, use_sudo=False, mode=0755)
    put('shmig', INSTALL_ROOT, use_sudo=False, mode=0755)
    put('conf/{0}'.format(target), INSTALL_ROOT, use_sudo=False)


def users():
    put("users.sh", "/root/users.sh", use_sudo=True)
    sudo('chmod 750 /root/users.sh && /root/users.sh')


def docker():
    sudo('''apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common''')
    run('''curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -''')
    sudo('''sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"''')
    sudo('''apt-get update && apt-get install -y docker-ce''')

def os_prep():
    sudo('''sed -i 's/main$/main contrib non-free/g' /etc/apt/sources.list; apt-get update''')
    sudo('''apt-get -y install locales && printf "pl_PL.UTF-8 UTF-8\nen_US.UTF-8 UTF-8" > /etc/locale.gen && dpkg-reconfigure -f noninteractive locales''')
    docker()
    sudo('''docker network create -d bridge --subnet 192.168.216.0/24 --gateway 192.168.216.1 vmail || true''')
    __upload('logrotate.d')
    sudo('install -o root -g root -m 644 {0}/logrotate.d/rsyslog /etc/logrotate.d/rsyslog'.format(INSTALL_ROOT))


def packages():
    sudo('''echo -e '#!/bin/bash\nexit 101' > /usr/sbin/policy-rc.d && chmod a+x /usr/sbin/policy-rc.d \
        && DEBIAN_FRONTEND=noninteractive apt-get install -y \
            gpg unrar p7zip unzip rpm arj zoo p7zip-full cabextract lzop lrzip nomarch unrar-free \
            ca-certificates ssl-cert dnsutils \
            postfix postfix-pgsql postfix-policyd-spf-python postfix-pcre postgrey \
            dovecot-imapd dovecot-pgsql dovecot-sieve dovecot-managesieved dovecot-lmtpd dovecot-pop3d \
            amavisd-new libdbd-pg-perl spamassassin libnet-dns-perl libmail-spf-perl pyzor razor \
            clamav clamav-daemon clamav-freshclam \
            opendkim opendkim-tools \
            imapproxy \
            ntp \
            postgresql-9.6 \
            docker-ce \
            nginx \
            certbot python-certbot-nginx \
            rsyslog logrotate \
        && rm -f /usr/sbin/policy-rc.d ''')
    sudo('''test -L /etc/ssl/private/mail.pem || ln -s /etc/ssl/certs/ssl-cert-snakeoil.pem /etc/ssl/private/mail.pem''')
    sudo('''test -L /etc/ssl/private/mail.key || ln -s /etc/ssl/private/ssl-cert-snakeoil.key /etc/ssl/private/mail.key''')


def postgres():
    __upload('postgres')
    with cd(INSTALL_ROOT + '/postgres'):
        sudo("bash make.sh")


def postfix():
    __upload('postfix')
    with cd(INSTALL_ROOT + '/postfix'):
        sudo('bash make.sh')
    sudo('systemctl restart postfix')


def dovecot():
    __upload('dovecot')
    with cd(INSTALL_ROOT + '/dovecot'):
        sudo('bash make.sh')
    sudo('systemctl restart dovecot')


def amavis():
    __upload('amavis')
    with cd(INSTALL_ROOT + '/amavis'):
        sudo('bash make.sh')
    sudo('systemctl restart amavis')
    sudo("""su - amavis -c 'razor-admin -create' && test -L /var/lib/amavis/.razor/identity || su - amavis -c 'razor-admin -register'""")


def clamav():
    __upload('clamav')
    with cd(INSTALL_ROOT + '/clamav'):
        sudo('bash make.sh')
    sudo('systemctl restart clamav-freshclam')
    sudo('systemctl start clamav-daemon')
    

def spamassassin():
    __upload('spamassassin')
    with cd(INSTALL_ROOT + '/spamassassin'):
        sudo('bash make.sh')
    sudo('systemctl restart spamassassin')


def imapproxy():
    __upload('imapproxy')
    with cd(INSTALL_ROOT + '/imapproxy'):
        sudo('bash make.sh')
    sudo('systemctl restart imapproxy')


def opendkim():
    __upload('opendkim')
    with cd(INSTALL_ROOT + '/opendkim'):
        sudo('bash make.sh')
    sudo('systemctl restart opendkim')


def genkey(selector, domain):
    __upload('opendkim')
    with cd(INSTALL_ROOT + '/opendkim'):
        sudo('bash gen_key.sh {0} {1}'.format(selector, domain))
    sudo('systemctl restart opendkim')


def nginx():
    __upload('nginx')
    with cd(INSTALL_ROOT + '/nginx'):
        sudo('bash make.sh')
    sudo('systemctl restart nginx')


def postfixadmin():
    __upload('postfixadmin')
    with cd(INSTALL_ROOT + '/postfixadmin'):
        sudo('bash make.sh')
    sudo('/etc/postfixadmin/run.sh')


def roundcube():
    __upload('roundcube')
    with cd(INSTALL_ROOT + '/roundcube'):
        sudo('bash make.sh')
    sudo('/etc/roundcube/run.sh')


def certbot():
    __upload('certbot')
    with cd(INSTALL_ROOT + '/certbot'):
        sudo('bash make.sh')
    

def all():
    users()
    packages()
    certbot()
    opendkim()
    postgres()
    spamassassin()
    clamav()
    amavis()
    dovecot()
    postfix()
    imapproxy()
    nginx()
    postfixadmin()
    roundcube()
